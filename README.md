jupyter-notebook-quickdaemon
==========

These scripts allow to launch an instance of [jupyter notebook](https://github.com/jupyter/notebook) and to open a throwaway notebook directly in the browser with a single command for a quick and powerful python test or code environment.

The jupyter instance will remain active and is manageable with systemd.
Subsequent calls to the script will open the same notebook in the browser.

BEWARE! By default the scripts copy an empty notebook into `/tmp` and work with that, requiring you to manually save your work, if you intend to keep it. This is because these scripts are intended as a temporary environment, but this behaviour can be easily modified if needed.

Installation
-------
- Copy `jn` and `jn-inner` in your path (for example `.local/bin`)
- Copy `jn.service` in your systemd user service folder (for example `.config/systemd/user`) replacing `$JN-INNER\_PATH$` with the correct jn-inner script path
- Copy `Testing.ipynb` to the same folder specified in jn and jn-inner (something like `.local/share/jn`)

License
-------

This software is released under the GPL v3.0 licence.

Contributions
-------

Any contribution is welcome! :D
